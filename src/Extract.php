<?php
/**
 * TLDExtract: Library for parsing of domain names, in particular TLD analysis.  Uses Public Suffix List.
 *
 * @link https://github.com/CrowdTwist/TLDExtract
 *
 * @copyright Copyright (c) 2016, Alexander Fedyashov and CrowdTwist LLC
 * @license https://raw.githubusercontent.com/CrowdTwist/TLDExtract/master/LICENSE Apache 2.0 License
 */

namespace CrowdTwist\TLDExtract;

use CrowdTwist\TLDExtract\Exceptions\RuntimeException;
use LayerShifter\TLDDatabase\Store;
use LayerShifter\TLDSupport\Helpers\Arr;
use LayerShifter\TLDSupport\Helpers\IP;
use LayerShifter\TLDSupport\Helpers\Str;

/**
 * Extract class accurately extracts subdomain, domain and TLD components from URLs.
 *
 * @see Result for more information on the returned data structure.
 */
class Extract
{

    /**
     * @const int If this option provided, extract will consider ICANN suffixes.
     */
    const MODE_ALLOW_ICANN = 2;

    /**
     * @const int If this option provided, extract will consider private suffixes.
     */
    const MODE_ALLOW_PRIVATE = 4;

    /**
     * @const int If this option provided, extract will consider custom domains.
     */
    const MODE_ALLOW_NOT_EXISTING_SUFFIXES = 8;

    /**
     * @const string RFC 3986 compliant scheme regex pattern.
     * @see https://tools.ietf.org/html/rfc3986#section-3.1
     */
    const SCHEMA_PATTERN = '#^([a-zA-Z][a-zA-Z0-9+\-.]*:)?//#';

    /**
     * @var int Value of extraction options.
     */
    private $extractionMode;

    /**
     * @var string Name of class that will store results of parsing.
     */
    private $resultClassName;

    /**
     * @var IDN Object of TLDExtract\IDN class.
     */
    private $idn;

    /**
     * @var Store Object of TLDDatabase\Store class.
     */
    private $suffixStore;

    /**
     * Factory constructor.
     *
     * @param null|string name of file with Public Suffix List database
     * @param null|string name of class that will store results of parsing
     * @param null|int bitmask of CrowdTwist\TLDExtract\Extract::MODE_* constants
     * setting the extraction modes to support
     * @throws RuntimeException
     */
    public function __construct($databaseFile = null, $resultClassName = null, $extractionMode = null)
    {
        $this->idn = new IDN;
        $this->suffixStore = new Store($databaseFile);

        // Checks for resultClassName argument.

        if (null !== $resultClassName) {
            if (!class_exists($resultClassName)) {
                throw new RuntimeException(sprintf('Class "%s" is not defined', $resultClassName));
            }

            if (!in_array(ResultInterface::class, class_implements($resultClassName), true)) {
                throw new RuntimeException(sprintf('Class "%s" not implements ResultInterface', $resultClassName));
            }

            $this->resultClassName = $resultClassName;
        } else {
            $this->resultClassName = Result::class;
        }

        $this->setExtractionMode($extractionMode);
    }

    /**
     * Sets the extraction mode.  This is a set of options that will determine
     * which TLDs are considered usable in the extraction process.
     *
     * @param int bitmask of CrowdTwist\TLDExtract\Extract::MODE_* constants;
     * defaults to all modes enabled
     * @return $this
     * @throws RuntimeException
     */
    public function setExtractionMode($extractionMode = null)
    {
        if (null === $extractionMode) {
            $this->extractionMode =
                static::MODE_ALLOW_ICANN
                | static::MODE_ALLOW_PRIVATE
                | static::MODE_ALLOW_NOT_EXISTING_SUFFIXES;
            return;
        }

        if (!is_int($extractionMode)) {
            throw new RuntimeException('Invalid argument type, extractionMode must be integer');
        }

        if (!in_array($extractionMode, [
            static::MODE_ALLOW_ICANN,
            static::MODE_ALLOW_PRIVATE,
            static::MODE_ALLOW_NOT_EXISTING_SUFFIXES,
            static::MODE_ALLOW_ICANN | static::MODE_ALLOW_PRIVATE,
            static::MODE_ALLOW_ICANN | static::MODE_ALLOW_NOT_EXISTING_SUFFIXES,
            static::MODE_ALLOW_ICANN | static::MODE_ALLOW_PRIVATE | static::MODE_ALLOW_NOT_EXISTING_SUFFIXES,
            static::MODE_ALLOW_PRIVATE | static::MODE_ALLOW_NOT_EXISTING_SUFFIXES
        ], true)
        ) {
            throw new RuntimeException(
                'Invalid argument type, extractionMode must be one of defined constants of their combination'
            );
        }

        $this->extractionMode = $extractionMode;
        return $this;
    }

    /**
     * @param int bitmask of CrowdTwist\TLDExtract\Extract::MODE_* constants;
     * defaults to all modes enabled
     */
    public function getExtractionMode()
    {
        return $this->extractionMode;
    }

    /**
     * Extract the subdomain, host and gTLD/ccTLD components from a URL.
     *
     * @param string URL that will be extracted
     * @return ResultInterface
     */
    public function parse($url)
    {
        $fqdn = $this->extractFqdn($url);

        // If received hostname is valid IP address, result will be formed from it.

        if (IP::isValid($fqdn)) {
            return new $this->resultClassName($fqdn, null, null);
        }

        list($hostname, $subdomain, $suffix) = $this->extractParts($fqdn);

        return new $this->resultClassName($hostname, $subdomain, $suffix);
    }

    /**
     * Method that extracts the domain name or IP address from a URL.
     *
     * @param string URL for extraction
     * @return null|string fully-qualified domain name or IP address
     */
    private function extractFqdn($url)
    {
        $url = trim(Str::lower($url));

        // Removes scheme and path i.e. "https://github.com/layershifter" to "github.com/layershifter".

        $url = preg_replace(static::SCHEMA_PATTERN, '', $url);

        // Removes path and query part of URL i.e. "example.com/query" to "example.com".

        $url = $this->fixQueryPart($url);
        $hostname = Arr::first(explode('/', $url, 2));

        // Removes username from URL i.e. user@github.com to github.com.

        $hostname = Arr::last(explode('@', $hostname));

        // Remove ports from hosts, also check for IPv6 literals like "[3ffe:2a00:100:7031::1]".
        //
        // @see http://www.ietf.org/rfc/rfc2732.txt

        $lastBracketPosition = Str::strrpos($hostname, ']');

        if ($lastBracketPosition !== false && Str::startsWith($hostname, '[')) {
            return Str::substr($hostname, 1, $lastBracketPosition - 1);
        }

        // This is either a normal hostname or an IPv4 address, just remove the port.

        $hostname = Arr::first(explode(':', $hostname));

        // If string is empty, null will be returned.

        return '' === $hostname ? null : $hostname;
    }

    /**
     * Extracts subdomain, host and suffix from input string. Based on logic described in
     * https://publicsuffix.org/list/.
     *
     * @param string a fully qualified domain name
     *
     * @return array<string|null> an array of hostname, subdomain, and suffix
     */
    public function extractParts($fqdn)
    {
        $suffix = $this->extractSuffix($fqdn);

        if ($suffix === $fqdn) {
            return [null, null, $suffix];
        }
        if (null === $suffix) {
            return [$fqdn, null, null];
        }

        $workingValue = Str::substr($fqdn, 0, -Str::length($suffix) - 1);

        $lastDot = Str::strrpos($workingValue, '.');

        if (false === $lastDot) {
            return [null, $workingValue, $suffix];
        }

        $hostname = Str::substr($workingValue, 0, $lastDot);
        $subdomain = Str::substr($workingValue, $lastDot + 1);

        return [
            $hostname,
            $subdomain,
            $suffix
        ];
    }

    /**
     * Extracts suffix from a domain name using Public Suffix List database.
     *
     * @param string a fully qualified domain name
     * @return null|string
     */
    private function extractSuffix($fqdn)
    {
        // If FQDN has leading dot, it's invalid.
        if (Str::startsWith($fqdn, '.')) {
            return null;
        }

        // If domain is in punycode, it will be converted to IDN.

        $isPunycoded = Str::strpos($fqdn, 'xn--') !== false;

        if ($isPunycoded) {
            $fqdn = $this->idn->toUTF8($fqdn);
        }

        $suffix = $this->parseSuffix($fqdn);

        if (null === $suffix) {
            if (!($this->extractionMode & static::MODE_ALLOW_NOT_EXISTING_SUFFIXES)) {
                return null;
            }

            $lastDot = Str::strrpos($fqdn, '.');
            if ($lastDot === false) {
                $suffix = $fqdn;
            } else {
                $suffix = Str::substr($fqdn, $lastDot + 1);
            }
        }

        // If domain is punycoded, suffix will be converted to punycode.

        return $isPunycoded ? $this->idn->toASCII($suffix) : $suffix;
    }

    /**
     * Extracts suffix from a domain name using Public Suffix List database.
     *
     * @param string a fully qualified domain name
     * @return null|string
     */
    private function parseSuffix($fqdn)
    {
        $fqdnParts = explode('.', $fqdn);
        $realSuffix = null;

        for ($i = 0, $count = count($fqdnParts); $i < $count; $i++) {
            $possibleSuffix = implode('.', array_slice($fqdnParts, $i));
            $exceptionSuffix = '!' . $possibleSuffix;

            if ($this->suffixExists($exceptionSuffix)) {
                $realSuffix = implode('.', array_slice($fqdnParts, $i + 1));
                break;
            }

            if ($this->suffixExists($possibleSuffix)) {
                $realSuffix = $possibleSuffix;
                break;
            }

            $wildcardTld = '*.' . implode('.', array_slice($fqdnParts, $i + 1));

            if ($this->suffixExists($wildcardTld)) {
                $realSuffix = $possibleSuffix;
                break;
            }
        }

        return $realSuffix;
    }

    /**
     * Method that checks existence of entry in Public Suffix List database,
     * taking into account the configured extraction mode.
     *
     * @param string entry to be checked for in the Public Suffix List database
     * @return bool
     */
    private function suffixExists($entry)
    {
        if (!$this->suffixStore->isExists($entry)) {
            return false;
        }

        $type = $this->suffixStore->getType($entry);

        if ($this->extractionMode & static::MODE_ALLOW_ICANN && $type === Store::TYPE_ICCAN) {
            return true;
        }

        return $this->extractionMode & static::MODE_ALLOW_PRIVATE && $type === Store::TYPE_PRIVATE;
    }

    /**
     * Fixes URL from "example.com?query" to "example.com/?query".
     *
     * @see https://github.com/layershifter/TLDExtract/issues/5
     * @param string URL
     * @return string
     */
    private function fixQueryPart($url)
    {
        $position = Str::strpos($url, '?');
        if ($position === false) {
            return $url;
        }
        return Str::substr($url, 0, $position) . '/' . Str::substr($url, $position);
    }

}
