<?php
/**
 * TLDExtract: Library for extraction of domain parts e.g. TLD. Domain parser that uses Public Suffix List.
 *
 * @link https://github.com/CrowdTwist/TLDExtract
 *
 * @copyright Copyright (c) 2016, Alexander Fedyashov and CrowdTwist LLC
 * @license https://raw.githubusercontent.com/CrowdTwist/TLDExtract/master/LICENSE Apache 2.0 License
 */

namespace CrowdTwist\TLDExtract;

/**
 * Interface for parsing results.
 */
interface ResultInterface
{

    /**
     * Class that implements ResultInterface must have following constructor.
     *
     * @param null|string $hostname
     * @param null|string $subdomain
     * @param null|string $suffix
     */
    public function __construct($hostname, $subdomain, $suffix);

    /**
     * Returns hostname if it exists.
     *
     * @return null|string
     */
    public function getHostname();

    /**
     * Return hostname parts if they exist.  For example, a parse result for
     * www.a.b.c.com should return ['www', 'a', 'b'].
     *
     * @return null|array
     */
    public function getHostnameParts();

    /**
     * Returns subdomain if it exists.
     *
     * @return null|string
     */
    public function getSubdomain();

    /**
     * Returns suffix if it exists.
     *
     * @return null|string
     */
    public function getSuffix();

    /**
     * Method that returns full host record.
     *
     * @return string
     */
    public function getFullHost();

    /**
     * Returns registrable domain or null.
     *
     * @return null|string
     */
    public function getRegistrableDomain();

    /**
     * Returns true if domain is valid.
     *
     * @return bool
     */
    public function isValidDomain();

    /**
     * Returns true is result is IP.
     *
     * @return bool
     */
    public function isIp();
}
