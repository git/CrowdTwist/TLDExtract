<?php
/**
 * TLDExtract: Library for extraction of domain parts e.g. TLD. Domain parser that uses Public Suffix List.
 *
 * @link https://github.com/CrowdTwist/TLDExtract
 *
 * @copyright Copyright (c) 2016, Alexander Fedyashov and CrowdTwist LLC
 * @license https://raw.githubusercontent.com/CrowdTwist/TLDExtract/master/LICENSE Apache 2.0 License
 */

namespace CrowdTwist\TLDExtract;

use LayerShifter\TLDSupport\Helpers\IP;

/**
 * This class holds the components of a domain name.
 *
 * You can access the components using either method, property or array syntax. For example, "echo $result->suffix" and
 * "echo $result['suffix']" will both work and output the same string.
 *
 * All properties are read-only.
 *
 * @property-read null|string $subdomain
 * @property-read null|string $hostname
 * @property-read null|string $suffix
 */
class Result implements \ArrayAccess, ResultInterface
{

    /**
     * Hostname part of domain or IP-address. For example, in "a.b.google.com" the hostname is "a.b".
     *
     * @var null|string
     */
    private $hostname;

    /**
     * The subdomain. For example, the subdomain of "a.b.google.com" is "google".
     *
     * @var null|string
     */
    private $subdomain;

    /**
     * The top-level domain / public suffix. For example: "com", "co.uk", "act.edu.au".
     *
     * @var null|string
     */
    private $suffix;

    /**
     * Constructor of class.
     *
     * @param null|string $hostname
     * @param null|string $subdomain
     * @param null|string $suffix
     */
    public function __construct($hostname, $subdomain, $suffix)
    {
        $this->hostname = $hostname;
        $this->subdomain = $subdomain;
        $this->suffix = $suffix;
    }

    /**
     * Returns hostname if it exists.
     *
     * @return null|string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * Return hostname parts if they exist.  For example, a parse result for
     * www.a.b.c.com should return ['www', 'a', 'b'].
     *
     * @return null|array
     */
    public function getHostnameParts()
    {
        return null === $this->hostname ? null : explode('.', $this->hostname);
    }

    /**
     * Returns subdomain if it exists.
     *
     * @return null|string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * Returns suffix if it exists.
     *
     * @return null|string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Method that returns full host record.
     *
     * @return string
     */
    public function getFullHost()
    {
        // Case 1: Host hasn't suffix, possibly IP.

        if (null === $this->suffix) {
            return $this->hostname;
        }

        // Case 2: Domain with suffix, but without hostname.

        if (null === $this->hostname) {
            return sprintf('%s.%s', $this->subdomain, $this->suffix);
        }

        // Case 3: Hostname with suffix and subdomain.

        return sprintf('%s.%s.%s', $this->hostname, $this->subdomain, $this->suffix);
    }

    /**
     * Returns registrable domain or null.
     *
     * @return null|string
     */
    public function getRegistrableDomain()
    {
        if (null === $this->suffix) {
            return null;
        }
        if (null === $this->subdomain) {
            return null;
        }
        return sprintf('%s.%s', $this->subdomain, $this->suffix);
    }

    /**
     * Returns public domain or null.
     *
     * @return null|string
     */
    public function getPublicDomain()
    {
        if (null === $this->suffix) {
            return null;
        }
        if (null === $this->subdomain) {
            return $this->suffix;
        }
        return sprintf('%s.%s', $this->subdomain, $this->suffix);
    }

    /**
     * Returns true if domain is valid.
     *
     * @return bool
     */
    public function isValidDomain()
    {
        return null !== $this->getRegistrableDomain();
    }

    /**
     * Returns true is result is IP.
     *
     * @return bool
     */
    public function isIp()
    {
        return null === $this->suffix && IP::isValid($this->hostname);
    }

    /**
     * Magic method for run isset on private params.
     *
     * @param string $name Field name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    /**
     * Converts class fields to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getFullHost();
    }

    /**
     * Whether or not an offset exists.
     *
     * @param mixed $offset An offset to check for
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    /**
     * Returns the value at specified offset.
     *
     * @param mixed $offset The offset to retrieve.
     *
     * @throws \OutOfRangeException
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->{$offset};
    }

    /**
     * Magic method, controls access to private params.
     *
     * @param string $name Name of params to retrieve
     *
     * @throws \OutOfRangeException
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (!property_exists($this, $name)) {
            throw new \OutOfRangeException(sprintf('Unknown field "%s"', $name));
        }

        return $this->{$name};
    }

    /**
     * Magic method, makes params read-only.
     *
     * @param string $name  Name of params to retrieve
     * @param mixed  $value Value to set
     *
     * @throws \LogicException
     *
     * @return void
     */
    public function __set($name, $value)
    {
        throw new \LogicException("Can't modify an immutable object.");
    }

    /**
     * Disables assigns a value to the specified offset.
     *
     * @param mixed $offset The offset to assign the value to
     * @param mixed $value  Value to set
     *
     * @throws \LogicException
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        throw new \LogicException(
            sprintf("Can't modify an immutable object. You tried to set value '%s' to field '%s'.", $value, $offset)
        );
    }

    /**
     * Disables unset of an offset.
     *
     * @param mixed $offset The offset for unset
     *
     * @throws \LogicException
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        throw new \LogicException(sprintf("Can't modify an immutable object. You tried to unset '%s.'", $offset));
    }

    /**
     * Get the domain name components as a native PHP array.  The returned
     * array will contain the keys 'hostname', 'subdomain', and 'suffix'.
     *
     * @return array<string:string|null>
     */
    public function toArray()
    {
        return [
            'hostname' => $this->hostname,
            'subdomain' => $this->subdomain,
            'suffix' => $this->suffix,
        ];
    }

    /**
     * Get the domain name components as a JSON string.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

}
