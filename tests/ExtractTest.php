<?php
/**
 * TLDExtract: Library for extraction of domain parts e.g. TLD. Domain parser that uses Public Suffix List.
 *
 * @link https://github.com/CrowdTwist/TLDExtract
 *
 * @copyright Copyright (c) 2016, Alexander Fedyashov
 * @license https://raw.githubusercontent.com/CrowdTwist/TLDExtract/master/LICENSE Apache 2.0 License
 */

namespace CrowdTwist\TLDExtract\Tests;

use CrowdTwist\TLDExtract\Exceptions\RuntimeException;
use CrowdTwist\TLDExtract\Extract;
use CrowdTwist\TLDExtract\Result;
use LayerShifter\TLDDatabase\Store;

/**
 * Tests for Extract class.
 */
class ExtractTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Extract Object of Extract class
     */
    private $extract;

    /**
     * Bootstrap method.
     */
    protected function setUp()
    {
        $this->extract = new Extract();

        parent::setUp();
    }

    /**
     * Tests constructor().
     *
     * @void
     */
    public function testConstructor()
    {
        $defaultMode = Extract::MODE_ALLOW_ICANN
            | Extract::MODE_ALLOW_PRIVATE
            | Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES;

        // Variant 1.

        $extract = new Extract();

        static::assertAttributeInstanceOf(Store::class, 'suffixStore', $extract);
        static::assertAttributeEquals($defaultMode, 'extractionMode', $extract);
        static::assertAttributeEquals(Result::class, 'resultClassName', $extract);

        // Variant 2.

        $extract = new Extract(__DIR__ . '/sample-database.php');

        static::assertAttributeInstanceOf(Store::class, 'suffixStore', $extract);
        static::assertAttributeEquals($defaultMode, 'extractionMode', $extract);
        static::assertAttributeEquals(Result::class, 'resultClassName', $extract);

        // Variant 3.

        $extract = new Extract(null, SampleResult::class);

        static::assertAttributeInstanceOf(Store::class, 'suffixStore', $extract);
        static::assertAttributeEquals($defaultMode, 'extractionMode', $extract);
        static::assertAttributeEquals(SampleResult::class, 'resultClassName', $extract);

        // Variant 4.

        $extract = new Extract(null, null, Extract::MODE_ALLOW_ICANN);

        static::assertAttributeInstanceOf(Store::class, 'suffixStore', $extract);
        static::assertAttributeEquals(Extract::MODE_ALLOW_ICANN, 'extractionMode', $extract);
        static::assertAttributeEquals(Result::class, 'resultClassName', $extract);
    }

    /**
     * Tests constructor() exception for non-existence result class.
     *
     * @void
     */
    public function testConstructorNonExistenceClass()
    {
        $this->setExpectedException(RuntimeException::class, 'is not defined');
        new Extract(null, 'NonExistenceClass');
    }

    /**
     * Tests constructor() exception for result class that not implements result interface.
     *
     * @void
     */
    public function testConstructorNotImplements()
    {
        $this->setExpectedException(RuntimeException::class, 'not implements ResultInterface');
        new Extract(null, Extract::class);
    }

    /**
     * Tests setExtractionMode() method.
     *
     * @void
     */
    public function testSetExtractionMode()
    {
        $extract = new Extract();

        // Variant 1.

        $extract->setExtractionMode(null);
        static::assertAttributeEquals(
            Extract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_PRIVATE | Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES,
            'extractionMode',
            $extract
        );

        // Variant 2.

        $extract->setExtractionMode(Extract::MODE_ALLOW_ICANN);
        static::assertAttributeEquals(Extract::MODE_ALLOW_ICANN, 'extractionMode', $extract);

        // Variant 3.

        $extract->setExtractionMode(Extract::MODE_ALLOW_PRIVATE);
        static::assertAttributeEquals(Extract::MODE_ALLOW_PRIVATE, 'extractionMode', $extract);

        // Variant 4.

        $extract->setExtractionMode(Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES);
        static::assertAttributeEquals(Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES, 'extractionMode', $extract);

        // Variant 5.

        $extract->setExtractionMode(Extract::MODE_ALLOW_PRIVATE | Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES);
        static::assertAttributeEquals(
            Extract::MODE_ALLOW_PRIVATE | Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES,
            'extractionMode',
            $extract
        );
    }

    /**
     * Tests setExtractionMode() exception for invalid mode type.
     *
     * @void
     */
    public function testSetExtractionModeInvalidArgumentType()
    {
        $this->setExpectedException(RuntimeException::class, 'Invalid argument type, extractionMode must be integer');

        $extract = new Extract();
        $extract->setExtractionMode('a');
    }

    /**
     * Tests setExtractionMode() exception for invalid mode value.
     *
     * @void
     */
    public function testSetExtractionModeInvalidArgumentValue()
    {
        $this->setExpectedException(
            RuntimeException::class,
            'Invalid argument type, extractionMode must be one of defined constants'
        );

        $extract = new Extract();
        $extract->setExtractionMode(-10);
    }

    /**
     * Tests parsing result.
     *
     * @param string FQDN or URL for parsing
     * @param string expected registrable domain
     * @param string expected public domain (defaults to same as registrable)
     */
    private function checkDomain($check, $expectedRegistrable, $expectedPublic = null)
    {
        $result = $this->extract->parse($check);
        $registrableDomain = $result->getRegistrableDomain();
        static::assertSame($expectedRegistrable, $registrableDomain);
        $publicDomain = $result->getPublicDomain();
        static::assertSame($expectedPublic ?: $expectedRegistrable, $publicDomain);
    }

    /**
     * Real world test cases using official test data.
     *
     * @see http://mxr.mozilla.org/mozilla-central/source/netwerk/test/unit/data/test_psl.txt?raw=1
     * @copyright Public Domain/CC-0.  https://creativecommons.org/publicdomain/zero/1.0/
     */
    public function testParse()
    {
        // null input.

        $this->checkDomain(null, null);

        // Mixed case.

        $this->checkDomain('COM', null, 'com');
        $this->checkDomain('example.COM', 'example.com');
        $this->checkDomain('WwW.example.COM', 'example.com');

        // Leading dot.

        $this->checkDomain('.com', null);
        $this->checkDomain('.example', null);
        $this->checkDomain('.example.com', null);
        $this->checkDomain('.example.example', null);

        // Unlisted TLD.

        $this->checkDomain('example', null, 'example');
        $this->checkDomain('example.example', 'example.example');
        $this->checkDomain('b.example.example', 'example.example');
        $this->checkDomain('a.b.example.example', 'example.example');

        // TLD with only 1 rule.

        $this->checkDomain('biz', null, 'biz');
        $this->checkDomain('domain.biz', 'domain.biz');
        $this->checkDomain('b.domain.biz', 'domain.biz');
        $this->checkDomain('a.b.domain.biz', 'domain.biz');

        // TLD with some 2-level rules.

        $this->checkDomain('com', null, 'com');
        $this->checkDomain('example.com', 'example.com');
        $this->checkDomain('b.example.com', 'example.com');
        $this->checkDomain('a.b.example.com', 'example.com');
        $this->checkDomain('uk.com', null, 'uk.com');
        $this->checkDomain('example.uk.com', 'example.uk.com');
        $this->checkDomain('b.example.uk.com', 'example.uk.com');
        $this->checkDomain('a.b.example.uk.com', 'example.uk.com');
        $this->checkDomain('test.ac', 'test.ac');

        // TLD with only 1 (wildcard) rule.

        $this->checkDomain('mm', null, 'mm');
        $this->checkDomain('c.mm', null, 'c.mm');
        $this->checkDomain('b.c.mm', 'b.c.mm');
        $this->checkDomain('a.b.c.mm', 'b.c.mm');

        // TLD with wildcards

        $this->checkDomain('com', null, 'com');
        $this->checkDomain('example.com', 'example.com');
        $this->checkDomain('b.example.com', 'example.com');
        $this->checkDomain('a.b.example.com', 'example.com');
        $this->checkDomain('uk.com', null, 'uk.com');
        $this->checkDomain('example.uk.com', 'example.uk.com');
        $this->checkDomain('b.example.uk.com', 'example.uk.com');
        $this->checkDomain('a.b.example.uk.com', 'example.uk.com');
        $this->checkDomain('test.ac', 'test.ac');

        // More complex TLD.

        $this->checkDomain('jp', null, 'jp');
        $this->checkDomain('test.jp', 'test.jp');
        $this->checkDomain('www.test.jp', 'test.jp');
        $this->checkDomain('ac.jp', null, 'ac.jp');
        $this->checkDomain('test.ac.jp', 'test.ac.jp');
        $this->checkDomain('www.test.ac.jp', 'test.ac.jp');
        $this->checkDomain('kyoto.jp', null, 'kyoto.jp');
        $this->checkDomain('test.kyoto.jp', 'test.kyoto.jp');
        $this->checkDomain('ide.kyoto.jp', null, 'ide.kyoto.jp');
        $this->checkDomain('b.ide.kyoto.jp', 'b.ide.kyoto.jp');
        $this->checkDomain('a.b.ide.kyoto.jp', 'b.ide.kyoto.jp');
        $this->checkDomain('c.kobe.jp', null, 'c.kobe.jp');
        $this->checkDomain('b.c.kobe.jp', 'b.c.kobe.jp');
        $this->checkDomain('a.b.c.kobe.jp', 'b.c.kobe.jp');
        $this->checkDomain('city.kobe.jp', 'city.kobe.jp');
        $this->checkDomain('www.city.kobe.jp', 'city.kobe.jp');

        // Complex case with wildcards inside static subdomains.

        $this->checkDomain('amazonaws.com', 'amazonaws.com', 'amazonaws.com');
        $this->checkDomain('s3.amazonaws.com', 'amazonaws.com', 'amazonaws.com');
        $this->checkDomain('example.s3.amazonaws.com', null, 'example.s3.amazonaws.com');
        $this->checkDomain('c.example.s3.amazonaws.com', 'c.example.s3.amazonaws.com');
        $this->checkDomain('b.c.example.s3.amazonaws.com', 'c.example.s3.amazonaws.com');
        $this->checkDomain('a.b.c.example.s3.amazonaws.com', 'c.example.s3.amazonaws.com');

        // TLD with a wildcard rule and exceptions.

        $this->checkDomain('ck', null, 'ck');
        $this->checkDomain('test.ck', null, 'test.ck');
        $this->checkDomain('b.test.ck', 'b.test.ck');
        $this->checkDomain('a.b.test.ck', 'b.test.ck');
        $this->checkDomain('www.ck', 'www.ck');
        $this->checkDomain('www.www.ck', 'www.ck');

        // US K12.

        $this->checkDomain('us', null, 'us');
        $this->checkDomain('test.us', 'test.us');
        $this->checkDomain('www.test.us', 'test.us');
        $this->checkDomain('ak.us', null, 'ak.us');
        $this->checkDomain('test.ak.us', 'test.ak.us');
        $this->checkDomain('www.test.ak.us', 'test.ak.us');
        $this->checkDomain('k12.ak.us', null, 'k12.ak.us');
        $this->checkDomain('test.k12.ak.us', 'test.k12.ak.us');
        $this->checkDomain('www.test.k12.ak.us', 'test.k12.ak.us');
    }

    /**
     * Real world test cases for IDN using official test data.
     *
     * @see http://mxr.mozilla.org/mozilla-central/source/netwerk/test/unit/data/test_psl.txt?raw=1
     * @copyright Public Domain/CC-0, https://creativecommons.org/publicdomain/zero/1.0/
     */
    public function testParseIdn()
    {
        // IDN labels.

        $this->checkDomain('食狮.com.cn', '食狮.com.cn');
        $this->checkDomain('食狮.公司.cn', '食狮.公司.cn');
        $this->checkDomain('www.食狮.公司.cn', '食狮.公司.cn');
        $this->checkDomain('shishi.公司.cn', 'shishi.公司.cn');
        $this->checkDomain('公司.cn', null, '公司.cn');
        $this->checkDomain('食狮.中国', '食狮.中国');
        $this->checkDomain('www.食狮.中国', '食狮.中国');
        $this->checkDomain('shishi.中国', 'shishi.中国');
        $this->checkDomain('中国', null, '中国');

        // Same as above, but punycoded.

        $this->checkDomain('xn--85x722f.com.cn', 'xn--85x722f.com.cn');
        $this->checkDomain('xn--85x722f.xn--55qx5d.cn', 'xn--85x722f.xn--55qx5d.cn');
        $this->checkDomain('www.xn--85x722f.xn--55qx5d.cn', 'xn--85x722f.xn--55qx5d.cn');
        $this->checkDomain('shishi.xn--55qx5d.cn', 'shishi.xn--55qx5d.cn');
        $this->checkDomain('xn--55qx5d.cn', null, 'xn--55qx5d.cn');
        $this->checkDomain('xn--85x722f.xn--fiqs8s', 'xn--85x722f.xn--fiqs8s');
        $this->checkDomain('www.xn--85x722f.xn--fiqs8s', 'xn--85x722f.xn--fiqs8s');
        $this->checkDomain('shishi.xn--fiqs8s', 'shishi.xn--fiqs8s');
        $this->checkDomain('xn--fiqs8s', null, 'xn--fiqs8s');
    }

    /**
     * Tests the suffix result from parsing.
     *
     * @param string FQDN or URL to parse
     * @param string expected suffix result from parsing
     * @param int extraction mode to apply to parsing
     */
    private function checkSuffix($fqdn, $expectedSuffix, $extractionMode = null)
    {
        if ($extractionMode === null) {
            $result = $this->extract->parse($fqdn);
        } else {
            $oldExtractionMode = $this->extract->getExtractionMode();
            $this->extract->setExtractionMode($extractionMode);
            $result = $this->extract->parse($fqdn);
            $this->extract->setExtractionMode($oldExtractionMode);
        }
        static::assertEquals($expectedSuffix, $result->getSuffix());
    }

    /**
     * Tests for URL parsing.
     */
    public function testParseUrls()
    {
        // Base tests.

        $this->checkSuffix('com', 'com');
        $this->checkSuffix('http://www.bbc.co.uk/news/business', 'co.uk');
        $this->checkSuffix('http://ru.wikipedia.org/', 'org');
        $this->checkSuffix('http://example.com/?foo=bar', 'com');
        $this->checkSuffix('http://example.com?foo=bar', 'com');
        $this->checkSuffix('bcc.bccbcc', 'bccbcc');
        $this->checkSuffix('svadba.net.ru', 'net.ru');
        $this->checkSuffix('us.example.com', 'com');
        $this->checkSuffix('us.example.org', 'org');

        // Test different schemas.

        $this->checkDomain('//www.bbc.co.uk/news/business', 'bbc.co.uk');
        $this->checkSuffix('//www.bbc.co.uk/news/business', 'co.uk');
        $this->checkDomain('ftp://www.bbc.co.uk/news/business', 'bbc.co.uk');
        $this->checkSuffix('ftp://www.bbc.co.uk/news/business', 'co.uk');
        $this->checkSuffix('test.schema://example.com', 'com');

        // Test IDN.

        $this->checkDomain('http://Яндекс.РФ', 'яндекс.рф');
        $this->checkSuffix('http://Яндекс.РФ', 'рф');

        // Test non-existent suffixes.

        $this->checkSuffix('http://localhost', 'localhost');
        $this->checkSuffix('http://localhost', null, Extract::MODE_ALLOW_ICANN);
        $this->checkSuffix('http://localhost', null, Extract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_PRIVATE);
        $this->checkSuffix('http://www.example.dev', 'dev');
        $this->checkSuffix('http://example.faketld', 'faketld');

        // Test IP.

        $this->checkSuffix('http://[::1]/', null);
        $this->checkSuffix('http://192.168.1.1/', null);
    }

    /**
     * Tests parsing result.
     *
     * @param string FQDN or URL to parse
     * @param string expected hostname result from parsing
     */
    private function checkHost($hostname, $expectedResult)
    {
        $result = $this->extract->parse($hostname);
        $hostname = $result->getHostname();
        static::assertEquals($expectedResult, $hostname);
    }

    /**
     * Tests for IP number parsing.
     */
    public function testParseIp()
    {
        // Test IPv4.

        $this->checkHost('http://192.168.1.1/', '192.168.1.1');
        $this->checkHost('http://127.0.0.1:443', '127.0.0.1');

        // Test IPv6.

        $this->checkHost('http://[2001:0:9d38:6abd:3431:eb:3cbd:22ba]/', '2001:0:9d38:6abd:3431:eb:3cbd:22ba');
        $this->checkHost('https://[2001:0:9d38:6abd:3431:eb:3cbd:22ba]:443/', '2001:0:9d38:6abd:3431:eb:3cbd:22ba');

        // Test local.

        $this->checkSuffix('http://[fe80::3%25eth0]', 'fe80::3%25eth0');
        $this->checkSuffix('http://[fe80::1%2511]', 'fe80::1%2511');
    }

    /**
     * Test for parse() withExtract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_PRIVATE options.
     */
    public function testParseOnlyExisting()
    {
        $extract = new Extract(null, null, Extract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_PRIVATE);

        static::assertNull($extract->parse('example.example')->getSuffix());
        static::assertNull($extract->parse('a.example.example')->getSuffix());
        static::assertNull($extract->parse('a.b.example.example')->getSuffix());
        static::assertNull($extract->parse('localhost')->getSuffix());
        static::assertNull($extract->parse('example.localhost')->getSuffix());

        static::assertEquals('com', $extract->parse('example.com')->getSuffix());
        static::assertEquals('com', $extract->parse('a.example.com')->getSuffix());
        static::assertEquals('example.com', $extract->parse('a.example.com')->getRegistrableDomain());
    }

    /**
     * Test for parse() with Extract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_PRIVATE options.
     */
    public function testParseDisablePrivate()
    {
        $extract = new Extract(null, null, Extract::MODE_ALLOW_ICANN | Extract::MODE_ALLOW_NOT_EXISTING_SUFFIXES);

        static::assertEquals('example', $extract->parse('example')->getSuffix());
        static::assertEquals('example', $extract->parse('example.example')->getSuffix());
        static::assertEquals('example', $extract->parse('a.example.example')->getSuffix());
        static::assertEquals('example', $extract->parse('a.b.example.example')->getSuffix());
        static::assertEquals('localhost', $extract->parse('example.localhost')->getSuffix());
        static::assertEquals('localhost', $extract->parse('localhost')->getSuffix());

        static::assertEquals('com', $extract->parse('example.com')->getSuffix());
        static::assertEquals('com', $extract->parse('a.example.com')->getSuffix());
        static::assertEquals('example.com', $extract->parse('a.example.com')->getRegistrableDomain());

        static::assertEquals('com', $extract->parse('a.blogspot.com')->getSuffix());
        static::assertEquals('com', $extract->parse('a.b.blogspot.com')->getSuffix());
        static::assertEquals('blogspot.com', $extract->parse('a.blogspot.com')->getRegistrableDomain());
    }

    /**
     * Test for parse() with MODE_ALLOW_ICANN option.
     */
    public function testParseICANNOption()
    {
        $extract = new Extract(null, null, Extract::MODE_ALLOW_ICANN);

        static::assertNull($extract->parse('example.example')->getSuffix());
        static::assertNull($extract->parse('a.example.example')->getSuffix());
        static::assertNull($extract->parse('a.b.example.example')->getSuffix());
        static::assertNull($extract->parse('localhost')->getSuffix());
        static::assertNull($extract->parse('example.localhost')->getSuffix());

        static::assertEquals('com', $extract->parse('example.com')->getSuffix());
        static::assertEquals('com', $extract->parse('a.example.com')->getSuffix());
        static::assertEquals('example.com', $extract->parse('a.example.com')->getRegistrableDomain());
        static::assertEquals('com', $extract->parse('a.blogspot.com')->getSuffix());
        static::assertEquals('com', $extract->parse('a.b.blogspot.com')->getSuffix());
        static::assertEquals('blogspot.com', $extract->parse('a.blogspot.com')->getRegistrableDomain());
    }

    /**
     * Test for fixQueryPart() method.
     */
    public function fixQueryPart()
    {
        $method = new \ReflectionMethod(Extract::class, 'fixQueryPart');
        $method->setAccessible(true);

        static::assertEquals('http://example.com/?query', $method->invoke($this->extract), 'http://example.com/?query');
        static::assertEquals('http://example.com/?query', $method->invoke($this->extract), 'http://example.com?query');
    }
}
