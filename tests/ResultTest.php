<?php
/**
 * PHP version 5.
 *
 * @category Exceptions
 *
 * @author   Alexander Fedyashov <a@fedyashov.com>
 * @license  MIT https://opensource.org/licenses/MIT
 *
 * @link     https://github.com/layershifter/TLDExtract
 */
namespace CrowdTwist\TLDExtract\Tests;

use CrowdTwist\TLDExtract\Extract;
use CrowdTwist\TLDExtract\Result;

/**
 * Test that coverages all cases of CrowdTwist\TLDExtract\Result.
 */
class ResultTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Object for tests.
     *
     * @var Result
     */
    private $entity;

    /**
     * Method that setups test's environment.
     */
    public function setUp()
    {
        $this->entity = new Result('192.168.0.1', null, null);
    }

    /**
     * Test for __constructor.
     */
    public function testConstruct()
    {
        static::assertNull($this->entity->subdomain);
        static::assertEquals('192.168.0.1', $this->entity->hostname);
        static::assertNull($this->entity->suffix);

        $entity = new Result(null, 'domain', 'com');

        static::assertNull($entity->hostname);
        static::assertEquals('domain', $entity->subdomain);
        static::assertEquals('com', $entity->suffix);

        unset($entity);

        $entity = new Result('www', 'domain', 'com');

        static::assertEquals('www', $entity->hostname);
        static::assertEquals('domain', $entity->subdomain);
        static::assertEquals('com', $entity->suffix);

        static::assertArrayHasKey('hostname', $entity);
        static::assertArrayHasKey('subdomain', $entity);
        static::assertArrayHasKey('suffix', $entity);
    }

    /**
     * Test domain entry.
     */
    public function testDomain()
    {
        $extract = new Extract;
        $result = $extract->parse('github.com');

        static::assertEquals('github.com', $result->getFullHost());
        static::assertEquals(null, $result->getHostname());
        static::assertEquals(null, $result->getHostnameParts());
        static::assertEquals('github.com', $result->getRegistrableDomain());
        static::assertEquals('github.com', $result->getPublicDomain());
        static::assertTrue($result->isValidDomain());
        static::assertFalse($result->isIp());
    }

    /**
     * Test subdomain entry.
     */
    public function testSubDomain()
    {
        $extract = new Extract;
        $result = $extract->parse('shop.github.com');

        static::assertEquals('shop.github.com', $result->getFullHost());
        static::assertEquals('github', $result->getSubdomain());
        static::assertEquals('github.com', $result->getRegistrableDomain());
        static::assertEquals('github.com', $result->getPublicDomain());
        static::assertTrue($result->isValidDomain());
        static::assertFalse($result->isIp());
    }

    /**
     * Test hostname parts.
     */
    public function testHostnameParts()
    {
        $extract = new Extract;
        $result = $extract->parse('new.shop.github.com');

        static::assertEquals('new.shop.github.com', $result->getFullHost());
        static::assertEquals('new.shop', $result->getHostname());
        static::assertEquals(['new', 'shop'], $result->getHostnameParts());
    }

    /**
     * Test IP entry.
     */
    public function testIp()
    {
        $extract = new Extract;
        $result = $extract->parse('192.168.0.1');

        static::assertEquals('192.168.0.1', $result->getFullHost());
        static::assertEquals(null, $result->getSubdomain());
        static::assertEquals(['192', '168', '0', '1'], $result->getHostnameParts());
        static::assertNull($result->getRegistrableDomain());
        static::assertFalse($result->isValidDomain());
        static::assertTrue($result->isIp());
    }

    /**
     * Test for toJson().
     */
    public function testToJson()
    {
        static::assertJsonStringEqualsJsonString(
            json_encode((object) [
                'hostname' => '192.168.0.1',
                'subdomain' => null,
                'suffix' => null,
            ]),
            $this->entity->toJson()
        );
    }

    /**
     * Test for magic method __toString().
     */
    public function testToString()
    {
        static::assertEquals('192.168.0.1', (string)$this->entity);
    }

    /**
     * Test for magic method __isset().
     */
    public function testIsset()
    {
        static::assertNull($this->entity->subdomain);
        static::assertNotNull($this->entity->hostname);
        static::assertNull($this->entity->suffix);

        /* @noinspection PhpUndefinedFieldInspection
         * Test for not existing field
         */
        static::assertEquals(false, isset($this->entity->test));
    }

    /**
     * Test for magic method __set().
     */
    public function testSet()
    {
        $this->setExpectedException('LogicException');
        $this->entity->offsetSet('hostname', 'another-domain');
    }

    /**
     * Test for magic method __set().
     */
    public function testSetViaProperty()
    {
        $this->setExpectedException('LogicException');
        $this->entity->hostname = 'another-domain';
    }

    /**
     * Test for magic method __get().
     */
    public function testGet()
    {
        $this->setExpectedException('OutOfRangeException');

        /* @noinspection PhpUndefinedFieldInspection
         * Test for not existing field
         */
        $this->entity->hostname1;
    }

    /**
     * Test for magic method __offsetSet().
     */
    public function testOffsetSet()
    {
        $this->setExpectedException('LogicException');
        $this->entity['hostname'] = 'another-domain';
    }

    /**
     * Test for magic method __offsetGet().
     */
    public function testOffsetGet()
    {
        static::assertEquals('192.168.0.1', $this->entity['hostname']);
    }

    /**
     * Test for magic method __offsetUnset().
     */
    public function testOffsetUnset()
    {
        $this->setExpectedException('LogicException');
        unset($this->entity['hostname']);
    }

}
