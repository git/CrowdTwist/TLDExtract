<?php
/**
 * TLDExtract: Library for extraction of domain parts e.g. TLD. Domain parser that uses Public Suffix List.
 *
 * @link https://github.com/CrowdTwist/TLDExtract
 *
 * @copyright Copyright (c) 2016, Alexander Fedyashov
 * @license https://raw.githubusercontent.com/CrowdTwist/TLDExtract/master/LICENSE Apache 2.0 License
 */

namespace CrowdTwist\TLDExtract\Tests;

use CrowdTwist\TLDExtract\Extract;

/**
 * Tests for static.php.
 */
class StaticTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Tests for tld_extract() function.
     *
     * @return void
     */
    public function testExtract()
    {
        $result = tld_extract('http://www.domain.com');

        static::assertEquals('www.domain.com', $result->getFullHost());
        static::assertEquals('domain.com', $result->getRegistrableDomain());

        $result = tld_extract('a.b.blogspot.com', Extract::MODE_ALLOW_ICANN);

        static::assertEquals('a.b.blogspot.com', $result->getFullHost());
        static::assertEquals('blogspot.com', $result->getRegistrableDomain());
        static::assertEquals('blogspot', $result->getSubdomain());
        static::assertEquals('a.b', $result->getHostname());
        static::assertEquals(['a', 'b'], $result->getHostnameParts());
        static::assertEquals('com', $result->getSuffix());
    }

}
