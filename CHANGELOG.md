# Changelog

## 2.0.0 - 2016-12-14

New features/fixes:
* CrowdTwist rewrite for consistency with standard DNS terminology, fixes to handling of wildcard TLDs, addition of 'public domain' concept in results

## 1.2.0 - 2016-11-17

New features:
* remove dependency on `intl` extension (#8).

## 1.1.1 - 2016-08-03

Fixes:
* issue #5 with handling query part of URL

## 1.1.0 - 2016-06-29

New features:
* `tld_extract()` function for simple usage;
* `setExtractionMode()` method on `Extract` class for setting extract options.

## 1.0.0 - 2016-06-20

New release with following features:
* IDN support;
* Database in separate weekly updatable package;
* Full test coverage.
